import requests
import json

url = 'https://restcountries.eu/rest/v2/all'


def getAllCountries():
	return requests.get('https://restcountries.eu/rest/v2/all').json()

def getReturnCountryDict(countryDict):
	returnKeys =['name','region','population']
	return {k : v for k, v in countryDict.items() if k in returnKeys}

def getCountries():
	all_countries = getAllCountries()
	return [getReturnCountryDict(country) for country in all_countries]

def _list_cont(param, region):
    def is_in_region(dict_item):
        return dict_item.get('region') == region
    listed_countries = filter(is_in_region, param)


def get_data():
    r = requests.get(url)
    dicts_list = json.loads(r.content)
    return dicts_list

def country_by_region(region):
    dicts_list = get_data()
    region_dicts = [item for item in dicts_list if item.get('region') == region]
    countries = [item.get('name') for item in region_dicts]
    print '\n'.join(countries)
    return countries

print get_data()
print country_by_region('Europe')


def continent_summary(country_dicts, continent_name):
    continent_countries = filter(lambda c: c['region'] == continent_name, country_dicts)
    res = {}
    res['area'] = sum([c['area'] for c in continent_countries if c['area'] != None])
    res['population'] = sum([c['population'] for c in continent_countries if c['population'] != None])
    res['count'] = len(continent_countries)
    return res

 
def continent_report(continent):
    continent_list = get_countries_by_continent(continent)
    countries_num = len(continent_list)
    total_population = sum([x['population'] for x in continent_list if x['population']])
    total_area = sum([x['area'] for x in continent_list if x['area']])
    return {'number': countries_num, 'population': total_population, 'area': total_area}
